package cz.swsamuraj.variance.array

trait Powerful
class Jedi extends Powerful
class Sith extends Powerful

object ArrayInvariance {
  def main(args: Array[String]): Unit = {
    val jedi: Array[Jedi] = Array(new Jedi)
    val powerfuls: Array[Powerful] = jedi
    powerfuls(0) = new Sith
    val hybrid: Jedi = jedi(0)
  }
}
