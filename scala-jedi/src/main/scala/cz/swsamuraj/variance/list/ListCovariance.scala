package cz.swsamuraj.variance.list

trait Powerful
class Jedi extends Powerful
class Sith extends Powerful

object ListCovariance {
  def main(args: Array[String]): Unit = {
    val jedi: List[Jedi] = List(new Jedi)
    val powerfuls: List[Powerful] = jedi
    val morePowerfuls = new Sith :: powerfuls
  }
}
