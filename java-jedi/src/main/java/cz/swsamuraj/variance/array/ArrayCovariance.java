package cz.swsamuraj.variance.array;

interface Powerful {}
class Jedi implements Powerful {}
class Sith implements Powerful {}

public class ArrayCovariance {
    public static void main(String[] args) {
        Jedi[] jedi = new Jedi[] { new Jedi() };
        Powerful[] powerfuls = jedi;
        powerfuls[0] = new Sith();
        Jedi hybrid = jedi[0];
    }
}
