package cz.swsamuraj.variance.immutable;

import com.google.common.collect.ImmutableList;

interface Powerful {}
class Jedi implements Powerful {}
class Sith implements Powerful {}

public class UnmodifiableList {
    public static void main(String[] args) {
        ImmutableList<Jedi> jedi = ImmutableList.of(new Jedi());
        // ImmutableList<Powerful> powerfuls = jedi;   // Error
        ImmutableList<Powerful> powerfuls =
                new ImmutableList.Builder<Powerful>()
                        .addAll(jedi)
                        .add(new Sith())
                        .build();
    }
}
