package cz.swsamuraj.variance.list;

import java.util.LinkedList;
import java.util.List;

interface Powerful {}
class Jedi implements Powerful {}
class Sith implements Powerful {}

public class ListInvariance {
    public static void main(String[] args) {
        List<Jedi> jedi = new LinkedList<>();
        jedi.add(new Jedi());

        // List<Powerful> powerfuls = jedi;   // Error
        List<Powerful> powerfuls = new LinkedList<>(jedi);
        powerfuls.add(new Sith());
    }
}
