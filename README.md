# Covariance & Contravariance Examples #

An example project for demonstration of the [Covariance and Contravariance](https://en.wikipedia.org/wiki/Covariance_and_contravariance_(computer_science)) concepts in different languages.

### How do I get up and running? ###

1. Go to the language specific project.
1. Follow the specific `README` there.

### Languages ###

* [Java](https://bitbucket.org/sw-samuraj/blog-variance/src/tip/java-jedi/)
* [Scala](https://bitbucket.org/sw-samuraj/blog-variance/src/tip/scala-jedi/)
* [Groovy](https://bitbucket.org/sw-samuraj/blog-variance/src/tip/groovy-jedi/)